/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.common.definition.cef.json.operation;

import com.fasterxml.jackson.core.JsonGenerator;
import org.openatom.ubml.model.common.definition.cef.collection.ValElementCollection;
import org.openatom.ubml.model.common.definition.cef.json.CefNames;
import org.openatom.ubml.model.common.definition.cef.json.SerializerUtils;
import org.openatom.ubml.model.common.definition.cef.operation.CommonOperation;
import org.openatom.ubml.model.common.definition.cef.operation.CommonValidation;

/**
 * The Json Serializer Of Common Validation
 *
 * @ClassName: CommonValSerializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class CommonValSerializer extends CommonOpSerializer {
    @Override
    protected void writeExtendCommonOpBaseProperty(JsonGenerator writer, CommonOperation info) {

    }

    @Override
    protected void writeExtendCommonOpSelfProperty(JsonGenerator writer, CommonOperation info) {
        CommonValidation val = (CommonValidation)info;
        SerializerUtils.writePropertyName(writer, CefNames.GET_EXECUTING_DATA_STATUS);
        writeGetExecutingDataStatus(writer, val.getGetExecutingDataStatus());
        writeRequestElements(writer, val.getRequestElements());
    }

    private void writeRequestElements(JsonGenerator writer, ValElementCollection childElementsIds) {
        SerializerUtils.writePropertyName(writer, CefNames.REQUEST_ELEMENTS);
        if (childElementsIds == null || childElementsIds.size() <= 0)
            return;
        SerializerUtils.WriteStartArray(writer);

        for (String childElementsId : childElementsIds)
            SerializerUtils.writePropertyValue_String(writer, childElementsId);

        SerializerUtils.WriteEndArray(writer);
    }
}
