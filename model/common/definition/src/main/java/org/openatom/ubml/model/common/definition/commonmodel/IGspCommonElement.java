/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.common.definition.commonmodel;

import org.openatom.ubml.model.common.definition.cef.IGspCommonField;
import org.openatom.ubml.model.common.definition.commonmodel.entity.element.ElementCodeRuleConfig;
import org.openatom.ubml.model.common.definition.commonmodel.entity.element.GspCommonAssociation;

/**
 * The Interface  Of Common Element
 *
 * @ClassName: IGspCommonElement
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public interface IGspCommonElement extends IGspCommonField {

    /**
     * @return The Bill Code Config Of The  Element
     */
    ElementCodeRuleConfig getBillCodeConfig();


    /**
     * @return The Column Id Of The Element
     */
    String getColumnID();

    /**
     * @param value The Column Id Of The Element
     */
    void setColumnID(String value);

    /**
     * @return The Element Is Custom Item
     */
    boolean getIsCustomItem();

    /**
     * @param value Set Wether The Element Is Custom Item
     */
    void setIsCustomItem(boolean value);


    /**
     * @return Belong Model Id Of The Element
     */
    String getBelongModelID();

    /**
     * @param value Belong Model Id To Set Of The Element
     */
    void setBelongModelID(String value);


    /**
     * @return Belong Object Of The Element
     */
    @Override
    IGspCommonObject getBelongObject();

    /**
     * @param value Belong Object To Set Of The Element
     */
    void setBelongObject(IGspCommonObject value);


    /**
     * @return The Element Is Readonly
     */
    boolean getReadonly();

    /**
     * @param value The Readonly Value To Set Of The Element
     */
    void setReadonly(boolean value);


    /**
     * @param absObject         The Parent Object Of The New Element,It`s Used When The Element Is  Not An Association  Element
     * @param parentAssociation The Parent Association  Of  The Element When The Element Is An Association Element
     * @return
     */
    IGspCommonElement clone(IGspCommonObject absObject, GspCommonAssociation parentAssociation);


    /**
     * @return The Association Type Name Of The Element
     */
    String getAssociationTypeName();


    /**
     * @return The Enum Type Name Of The Element
     */
    String getEnumTypeName();


}