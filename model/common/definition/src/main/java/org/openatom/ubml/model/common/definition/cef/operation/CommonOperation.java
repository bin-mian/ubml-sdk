/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.common.definition.cef.operation;

import org.openatom.ubml.model.common.definition.cef.util.Guid;

/**
 * The Definition Of Common Operation
 *
 * @ClassName: CommonOperation
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class CommonOperation implements Cloneable {
    /**
     * 物理主键
     */
    private String privateID;
    /**
     * 业务主键
     */
    private String privateCode;
    /**
     * 显示名称
     */
    private String privateName;
    /**
     * 描述
     */
    private String privateDescription;
    /**
     * 构件ID
     */
    private String privateComponentId;
    /**
     * 对应构件实体Name
     */
    private String privateComponentName;
    /**
     * 构件包名
     */
    private String privateComponentPkgName;
    /**
     * 是否引用操作
     */
    private boolean privateIsRef;
    /**
     * 是否生成构件
     */
    private boolean privateIsGenerateComponent;

    public CommonOperation() {
        // 20190529 新增属性IsGenerateComponent,标志操作生成=true,选择构件=false
        setIsGenerateComponent(true);
    }

    public final String getID() {
        return privateID;
    }

    public final void setID(String value) {
        privateID = value;
    }

    public final String getCode() {
        return privateCode;
    }

    public final void setCode(String value) {
        privateCode = value;
    }

    public final String getName() {
        return privateName;
    }

    public final void setName(String value) {
        privateName = value;
    }

    public final String getDescription() {
        return privateDescription;
    }

    public final void setDescription(String value) {
        privateDescription = value;
    }

    public final String getComponentId() {
        return privateComponentId;
    }

    public final void setComponentId(String value) {
        privateComponentId = value;
    }

    public final String getComponentName() {
        return privateComponentName;
    }

    public final void setComponentName(String value) {
        privateComponentName = value;
    }

    public final String getComponentPkgName() {
        return privateComponentPkgName;
    }

    public final void setComponentPkgName(String value) {
        privateComponentPkgName = value;
    }

    public final boolean getIsRef() {
        return privateIsRef;
    }

    public final void setIsRef(boolean value) {
        privateIsRef = value;
    }

    public final boolean getIsGenerateComponent() {
        return privateIsGenerateComponent;
    }

    public final void setIsGenerateComponent(boolean value) {
        privateIsGenerateComponent = value;
    }

    public CommonOperation clone() {
        try {
            CommonOperation op = (CommonOperation)super.clone();
            op.setID(Guid.newGuid().toString());
            return op;
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }
}