/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.lcm.metadata.common.extractor;

import com.inspur.edp.lcm.metadata.api.entity.extract.ExtractConfigration;
import com.inspur.edp.lcm.metadata.spi.ExtractAction;
import java.util.List;

public class ExtractHelper extends ExtractConfigLoader {
    private static ExtractHelper singleton = null;

    public static ExtractHelper getInstance() {
        if (singleton == null) {
            singleton = new ExtractHelper();
        }
        return singleton;
    }

    public List<ExtractConfigration> getExtractConfigrationList() {
        return loadExtractConfigration();
    }

    public ExtractAction getManager(String typeCode) {
        ExtractAction manager = null;
        ExtractConfigration extractConfigurationData = getExtractConfigurationData(typeCode);
        if (extractConfigurationData != null) {
            Class<?> cls;
            try {
                if (extractConfigurationData.getExtract() != null) {
                    cls = Class.forName(extractConfigurationData.getExtract().getName());
                    manager = (ExtractAction) cls.newInstance();
                }
            } catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
                e.printStackTrace();
                throw new RuntimeException(e);
            }
        }
        return manager;
    }
}
