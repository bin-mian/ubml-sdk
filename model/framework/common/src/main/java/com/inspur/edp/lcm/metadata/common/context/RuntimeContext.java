/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.lcm.metadata.common.context;

import io.iec.edp.caf.boot.context.CAFContext;

/**
 * Classname RuntimeContext Description TODO Date 2019/8/20 14:11
 *
 * @author zhongchq
 * @version 1.0
 */
public class RuntimeContext {

    public static String tenantID;
    private static String language;

    public static void setTenantID(String tenantID) {
        RuntimeContext.tenantID = tenantID;
    }

    public static void setLanguage(String language) {
        RuntimeContext.language = language;
    }

    public static String getTenantID() {

        if (tenantID == null) {
            tenantID = "9999";
        }
        return tenantID;
    }

    public static String getLanguage() {
        try {
            language = CAFContext.current.getLanguage();
        } catch (Exception e) {
            language = "zh-CHS";
        }
        return language;
    }
}
