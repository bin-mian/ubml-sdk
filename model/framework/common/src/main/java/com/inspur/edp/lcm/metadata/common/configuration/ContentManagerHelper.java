/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.lcm.metadata.common.configuration;

import com.inspur.edp.lcm.metadata.api.ConfigData.MetadataConfiguration;
import com.inspur.edp.lcm.metadata.spi.MetadataContentManager;
import java.util.Objects;

public class ContentManagerHelper extends MetadataConfigurationLoader {
    private static ContentManagerHelper sigleton = null;

    public ContentManagerHelper() {
    }

    public static ContentManagerHelper getInstance() {
        if (sigleton == null) {
            sigleton = new ContentManagerHelper();
        }
        return sigleton;
    }

    public MetadataContentManager getManager(String typeName) {
        MetadataContentManager manager = null;
        MetadataConfiguration metadataConfigurationData = getMetadataConfigurationData(typeName);
        if (!Objects.isNull(metadataConfigurationData) && !Objects.isNull(metadataConfigurationData.getManager())) {
            Class<?> managerClass;
            try {
                if (metadataConfigurationData.getManager() != null) {
                    managerClass = Class.forName(metadataConfigurationData.getManager().getName());
                    manager = (MetadataContentManager) managerClass.newInstance();
                }
            } catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
                e.printStackTrace();
                throw new RuntimeException(e);
            }
        }
        return manager;
    }
}
