/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.lcm.metadata.cache;

import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.lcm.metadata.api.entity.MetadataPackage;
import com.inspur.edp.lcm.metadata.api.entity.MetadataPackageReference;
import com.inspur.edp.lcm.metadata.api.entity.MetadataReference;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 元数据运行时缓存
 */
public class MetadataCacheManager {

    //元数据包基本信息
    private static final Map<String, MetadataPackage> MEDATA_PACKAGE_INFO = new HashMap<>();
    //元数据包清单列表信息
    private static final Map<String, List<GspMetadata>> METADATA_MANIFEST_INFO = new HashMap<>();
    //元数据基础信息
    private static final Map<String, GspMetadata> METADATA_INFO = new HashMap<>();
    //元数据依赖关系
    private static final Map<String, List> METADATA_DEPENDENCE = new HashMap<>();
    //元数据与元数据包名的映射关系
    private static final Map<String, String> METADATA_PATH_MAPPING = new HashMap<>();
    //所有的元数据包信息
    private static final Map<String, List<MetadataPackage>> ALL_METADATA_PACKAGE_INFO = new HashMap<>();
    //所有的元数据信息
    private static final Map<String, List<GspMetadata>> ALL_METADATA_INFO = new HashMap<>();
    private static Map<String, List<GspMetadata>> metadataListCache = new HashMap<>();
    private static final ReentrantLock LOCK = new ReentrantLock();

    MetadataCacheManager() {
    }

    public static void removeMetadataInfo(String key) {
        LOCK.lock();
        try {
            METADATA_INFO.remove(key);
        } finally {
            LOCK.unlock();
        }
    }

    public static Object getMetadataInfo(String metadataKey) {
        LOCK.lock();
        try {
            return METADATA_INFO.get(metadataKey);
        } finally {
            LOCK.unlock();
        }
    }

    public static void addMetadataInfo(String key, GspMetadata metadata) {
        LOCK.lock();
        try {
            METADATA_INFO.put(key, metadata);
        } finally {
            LOCK.unlock();
        }
    }

    public static void removeMDDependence(String key) {
        LOCK.lock();
        try {
            METADATA_DEPENDENCE.remove(key);
        } finally {
            LOCK.unlock();
        }
    }

    public static void addMDDependence(String key, List<MetadataReference> refs) {
        LOCK.lock();
        try {
            METADATA_DEPENDENCE.put(key, refs);
        } finally {
            LOCK.unlock();
        }
    }

    public static void removeMetadataPackageInfo(String key) {
        LOCK.lock();
        try {
            MEDATA_PACKAGE_INFO.remove(key);
        } finally {
            LOCK.unlock();
        }
    }

    public static void addMetadataPackageInfo(String key, MetadataPackage value) {
        LOCK.lock();
        try {
            MEDATA_PACKAGE_INFO.put(key, value);
        } finally {
            LOCK.unlock();
        }
    }

    public static Object getMetadataPackageInfo(String key) {
        LOCK.lock();
        try {
            return MEDATA_PACKAGE_INFO.get(key);
        } finally {
            LOCK.unlock();
        }
    }

    public static void removeMDManifestInfo(String key) {
        LOCK.lock();
        try {
            METADATA_MANIFEST_INFO.remove(key);
        } finally {
            LOCK.unlock();
        }
    }

    public static void addMDManifestInfo(String key, List<GspMetadata> value) {
        LOCK.lock();
        try {
            METADATA_MANIFEST_INFO.put(key, value);
        } finally {
            LOCK.unlock();
        }
    }

    public static Object getMDManifestInfo(String key) {
        LOCK.lock();
        try {
            return METADATA_MANIFEST_INFO.get(key);
        } finally {
            LOCK.unlock();
        }
    }

    public static void removeMPDependence(String key) {
        LOCK.lock();
        try {
            METADATA_DEPENDENCE.remove(key);
        } finally {
            LOCK.unlock();
        }
    }

    public static void addMPDependence(String key, List<MetadataPackageReference> value) {
        LOCK.lock();
        try {
            METADATA_DEPENDENCE.put(key, value);
        } finally {
            LOCK.unlock();
        }
    }

    public static Object getMetadataPathMapping(String key) {
        LOCK.lock();
        try {
            return METADATA_PATH_MAPPING.get(key);
        } finally {
            LOCK.unlock();
        }
    }

    public static void addMetadataPathMapping(String key, String value) {
        LOCK.lock();
        try {
            METADATA_PATH_MAPPING.put(key, value);
        } finally {
            LOCK.unlock();
        }
    }

    public static void removeMetadataPathMapping(String key) {
        LOCK.lock();
        try {
            METADATA_PATH_MAPPING.remove(key);
        } finally {
            LOCK.unlock();
        }
    }

    public static Object getAllMetadataPackageInfo(String key) {
        LOCK.lock();
        try {
            return ALL_METADATA_PACKAGE_INFO.get(key);
        } finally {
            LOCK.unlock();
        }
    }

    public static void addAllMetadataPackageInfo(String key, List<MetadataPackage> value) {
        LOCK.lock();
        try {
            ALL_METADATA_PACKAGE_INFO.put(key, value);
        } finally {
            LOCK.unlock();
        }
    }

    public static void removeAllMetadataPackageInfo(String key) {
        LOCK.lock();
        try {
            ALL_METADATA_PACKAGE_INFO.remove(key);
        } finally {
            LOCK.unlock();
        }
    }

    public static Object getAllMetadataInfo(String key) {
        LOCK.lock();
        try {
            return ALL_METADATA_INFO.get(key);
        } finally {
            LOCK.unlock();
        }
    }

    public static void addAllMetadataInfo(String key, List<GspMetadata> value) {
        LOCK.lock();
        try {
            ALL_METADATA_INFO.put(key, value);
        } finally {
            LOCK.unlock();
        }
    }

}