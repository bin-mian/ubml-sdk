/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.lcm.metadata.api.entity.bo;

import java.util.Date;

public class BOInfo {
    /// <summary>
    /// BO中项目数量
    /// <see cref="int"/>
    /// </summary>
    private int projectCount;

    /// <summary>
    /// BO中后端项目数量
    /// <see cref="int"/>
    /// </summary>
    private int backendProjectCount;

    /// <summary>
    /// BO中前端项目数量
    /// <see cref="int"/>
    /// </summary>
    private int frontendProjectCount;

    /// <summary>
    /// BO中项目最后修改时间
    /// <see cref="DateTime"/>
    /// </summary>
    private Date boProjectLastModifyTime;

    /// <summary>
    /// BO ID
    /// <see cref="string"/>
    /// </summary>
    private String id;

    /// <summary>
    /// BO Code
    /// <see cref="string"/>
    /// </summary>
    private String code;

    /// <summary>
    /// BO Name
    /// <see cref="string"/>
    /// </summary>
    private String name;

    public int getProjectCount() {
        return projectCount;
    }

    public void setProjectCount(int projectCount) {
        this.projectCount = projectCount;
    }

    public int getBackendProjectCount() {
        return backendProjectCount;
    }

    public void setBackendProjectCount(int backendProjectCount) {
        this.backendProjectCount = backendProjectCount;
    }

    public int getFrontendProjectCount() {
        return frontendProjectCount;
    }

    public void setFrontendProjectCount(int frontendProjectCount) {
        this.frontendProjectCount = frontendProjectCount;
    }

    public Date getBoProjectLastModifyTime() {
        return boProjectLastModifyTime;
    }

    public void setBoProjectLastModifyTime(Date boProjectLastModifyTime) {
        this.boProjectLastModifyTime = boProjectLastModifyTime;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    /// <summary>
    /// BO 描述
    /// <see cref="string"/>
    /// </summary>
    private String description;

}
