/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.lcm.metadata.shell;

import java.util.Queue;
import org.jline.reader.ParsedLine;
import org.jline.reader.Parser;
import org.springframework.shell.Input;
import org.springframework.shell.InputProvider;

/*
 * 读取命令
 */
public class StringInputProvider implements InputProvider {

    // 存储要执行的所有命令
    private Queue<String> commands;

    private final Parser parser;

    public StringInputProvider(Parser parser, Queue<String> commands) {
        this.parser = parser;
        this.commands = commands;
    }

    @Override
    public Input readInput() {
        String command = commands.poll();
        if (command == null) {
            // return null 时退出应用
            return null;
        } else {
            ParsedLine parsedLine = parser.parse(command, command.length());
            return new StringParsedLineInput(parsedLine);
        }
    }
}