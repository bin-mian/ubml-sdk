/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.lcm.metadata.servermanager.util;

/**
 * Classname MetadataUtils Description TODO Date 2019/8/20 16:43
 *
 * @author zhongchq
 * @version 1.0
 */
public class MetadataUtils {

    public static String manifestFileName = "manifest.json";

    public static String getManifestFileName() {
        return manifestFileName;
    }

    public static String getMetadataPackageExtension() {
        return ".mdpkg";
    }
}
