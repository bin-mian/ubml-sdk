/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.jittojava.core;

import com.inspur.edp.jittojava.context.entity.MavenDependency;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;

/**
 * @Classname JitUtils
 * @Description TODO
 * @Date 2019/12/16 16:30
 * @Created by liu_bintr
 * @Version 1.0
 */
public class JitUtils {
    public static String getArchetypePath() {
        return "platform/dev/main/libs/resources/archetypefront";
    }

    public static String getSpringPeopertiesPath() {
        return "resources/META-INF/spring.factories";
    }

    public static String getArchetypeArtifactId() {
        return "bf-df-bank-front-archetype";
    }

    public static String getArchetypeTarget() {
        return "target/" + getArchetypeArtifactId() + "-0.1.3.jar";
    }

    public static String getCompArchetypePath() {
        return "platform/dev/main/libs/resources/archetypefront-comp";
    }

    public static String getCompArchetypeArtifactId() {
        return "archetype-comp";
    }

    public static String getCompArchetypeTarget() {
        return "target/" + getCompArchetypeArtifactId() + "-0.1.3.jar";
    }

    public static Document readDocument(String filePath) {
        SAXReader sr = new SAXReader();
        try {
            return sr.read(filePath);
        } catch (DocumentException e) {
            throw new RuntimeException("无效路径" + filePath);
        }
    }

    public static ArrayList<MavenDependency> readExistDependencies(List<Element> existValues) {
        ArrayList<MavenDependency> list = new ArrayList<>();
        if (existValues == null || existValues.size() == 0) {
            return list;
        }
        for (int i = 0; i < existValues.size(); i++) {
            Element ele = (Element) existValues.get(i);
            list.add(new MavenDependency(getTagValue(ele, MavenDependencyConst.groupId), getTagValue(ele, MavenDependencyConst.artifactId),
                getTagValue(ele, MavenDependencyConst.version)));
        }
        return list;
    }

    public static boolean checkContainDependency(MavenDependency dependency, ArrayList<MavenDependency> dependencies) {
        for (MavenDependency item : dependencies) {
            if (item.equals(dependency)) {
                return true;
            }
        }
        return false;
    }

    public static void addDependencyDom(MavenDependency dependency, Element parentElement) {
        Element element = parentElement.addElement(MavenDependencyConst.dependency);
        Element gourpId = element.addElement(MavenDependencyConst.groupId);
        Element artifactId = element.addElement(MavenDependencyConst.artifactId);
        gourpId.setText(dependency.getGroupId());
        artifactId.setText(dependency.getArtifactId());
        if (dependency.getVersion() != null && !dependency.getVersion().isEmpty()) {
            Element version = element.addElement(MavenDependencyConst.version);
            version.setText(dependency.getVersion());
        }
    }

    public static void saveDocument(Document document, File xmlFile) {
        Writer osWrite = null;// 创建输出流
        try {
            osWrite = new OutputStreamWriter(new FileOutputStream(xmlFile));

            OutputFormat format = OutputFormat.createPrettyPrint(); // 获取输出的指定格式
            format.setEncoding("UTF-8");// 设置编码 ，确保解析的xml为UTF-8格式
            XMLWriter writer = new XMLWriter(osWrite, format);// XMLWriter
            // 指定输出文件以及格式
            writer.write(document);// 把document写入xmlFile指定的文件(可以为被解析的文件或者新创建的文件)
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static String getTagValue(Element ele, String tagName) {
        Element tagElement = (Element) ele.element(tagName);
        if (tagElement == null) {
            return null;
        }
        return tagElement.getText();
    }
}
