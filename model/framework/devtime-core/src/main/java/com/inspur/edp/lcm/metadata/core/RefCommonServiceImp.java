/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.lcm.metadata.core;

import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.lcm.metadata.api.service.RefCommonService;
import com.inspur.edp.lcm.metadata.common.Utils;
import com.inspur.edp.lcm.metadata.devcommon.ManagerUtils;

/**
 * @author zhaoleitr
 */
public class RefCommonServiceImp implements RefCommonService {
    private final RefCommonCoreService refService = new RefCommonCoreService();

    @Override
    public GspMetadata getRefMetadata(String metadataID) {
        String packagePath = ManagerUtils.getMetadataPackageLocation();
        String mavenPath = ManagerUtils.getMavenStoragePath();
        // 工程信息
        String path = Utils.getMetadataPath();
        if (path == null || path.isEmpty()) {
            throw new RuntimeException("元数据归属工程路径为空，无法找到元数据：" + metadataID);
        }
        String absolutePath = ManagerUtils.getAbsolutePath(path);
        GspMetadata gspMetadata = refService.getRefMetadata(metadataID, absolutePath, packagePath, mavenPath);

        String devRootPath = Utils.handlePath(ManagerUtils.getDevRootPath());
        if (gspMetadata.getRelativePath() != null && gspMetadata.getRelativePath().startsWith(devRootPath)) {
            gspMetadata.setRelativePath(gspMetadata.getRelativePath().substring(devRootPath.length()).substring(1));
        }
        return gspMetadata;
    }
}
