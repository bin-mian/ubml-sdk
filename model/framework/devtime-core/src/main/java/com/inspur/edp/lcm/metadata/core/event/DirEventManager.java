/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.lcm.metadata.core.event;

import com.inspur.edp.lcm.metadata.spi.event.DirEventListener;
import io.iec.edp.caf.commons.event.CAFEventArgs;
import io.iec.edp.caf.commons.event.EventManager;
import io.iec.edp.caf.commons.event.IEventListener;

public class DirEventManager extends EventManager {

    enum DirEventType {
        fireDirDeletingEvent,
        fireDirDeletedEvent
    }

    @Override
    public String getEventManagerName() {
        return "DirEventManager";
    }

    @Override
    public boolean isHandlerListener(IEventListener iEventListener) {
        return iEventListener instanceof DirEventListener;
    }

    @Override
    public void addListener(IEventListener iEventListener) {
        checkHandlerListener(iEventListener);
        DirEventListener dirEventListener = (DirEventListener) iEventListener;
        this.addEventHandler(DirEventType.fireDirDeletingEvent, dirEventListener, "fireDirDeletingEvent");
        this.addEventHandler(DirEventType.fireDirDeletedEvent, dirEventListener, "fireDirDeletedEvent");
    }

    @Override
    public void removeListener(IEventListener iEventListener) {
        checkHandlerListener(iEventListener);
        DirEventListener dirEventListener = (DirEventListener) iEventListener;
        this.removeEventHandler(DirEventType.fireDirDeletingEvent, dirEventListener, "fireDirDeletingEvent");
        this.removeEventHandler(DirEventType.fireDirDeletedEvent, dirEventListener, "fireDirDeletedEvent");
    }

    public void fireDirDeletingEvent(CAFEventArgs args) {
        this.fire(DirEventType.fireDirDeletingEvent, args);
    }

    public void fireDirDeletedEvent(CAFEventArgs args) {
        this.fire(DirEventType.fireDirDeletedEvent, args);
    }

    private void checkHandlerListener(IEventListener iEventListener) {
        if (!isHandlerListener(iEventListener)) {
            throw new RuntimeException("指定的监听者" + iEventListener.getClass().getName()
                + "没有实现" + DirEventListener.class.getName() + "接口");
        }
    }
}
