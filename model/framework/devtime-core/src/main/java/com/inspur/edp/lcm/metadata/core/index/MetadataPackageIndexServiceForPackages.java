/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.lcm.metadata.core.index;

import com.inspur.edp.lcm.metadata.api.entity.MetadataPackageForIndex;
import java.util.HashMap;
import java.util.Map;

public class MetadataPackageIndexServiceForPackages extends MetadataPackageIndexService {

    private static MetadataPackageIndexServiceForPackages instance;

    public static MetadataPackageIndexServiceForPackages getInstance(String location) {
        if (instance == null || (location != null && !location.equals(instance.location))) {
            instance = new MetadataPackageIndexServiceForPackages(location);
        }
        return instance;
    }

    private Map<String, MetadataPackageForIndex> metadataPackageIndexForPackagesHashMap;

    public Map<String, MetadataPackageForIndex> getMetadataPackageIndexForPackagesHashMap() {
        if (metadataPackageIndexForPackagesHashMap == null || metadataPackageIndexForPackagesHashMap.size() == 0) {
            refreshMetadataPackageIndex();
        }
        return metadataPackageIndexForPackagesHashMap;
    }

    public MetadataPackageIndexServiceForPackages(String location) {
        this.location = location;
        metadataPackageIndexHashMap = metadataPackageIndexForPackagesHashMap = new HashMap<>();
    }
}
