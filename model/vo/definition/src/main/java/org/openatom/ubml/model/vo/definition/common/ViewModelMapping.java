/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.vo.definition.common;

import java.io.Serializable;

/**
 * The Definition Of View Model Mapping
 *
 * @ClassName: ViewModelMapping
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class ViewModelMapping implements Cloneable, Serializable {
    /**
     * 映射类型
     */
    private MappingType privateMapType = MappingType.forValue(0);

    public final MappingType getMapType() {
        return privateMapType;
    }

    public final void setMapType(MappingType value) {
        privateMapType = value;
    }

    /**
     * 映射的对方元数据ContentId，用于唯一确定元数据
     */
    private String privateTargetMetadataId;

    public final String getTargetMetadataId() {
        return privateTargetMetadataId;
    }

    public final void setTargetMetadataId(String value) {
        privateTargetMetadataId = value;
    }

    ///// <summary>
    ///// 映射的对方元数据类型，BE或QO
    ///// </summary>
    //public MetadataType TargetMetadataType { get; set; }

    /**
     * 映射的对方元数据包名，用于唯一确定元数据
     */
    private String privateTargetMetadataPkgName;

    public final String getTargetMetadataPkgName() {
        return privateTargetMetadataPkgName;
    }

    public final void setTargetMetadataPkgName(String value) {
        privateTargetMetadataPkgName = value;
    }

    /**
     * 映射的元素ID，用于确定具体的元数据成员
     */
    private String privateTargetObjId;

    public final String getTargetObjId() {
        return privateTargetObjId;
    }

    public final void setTargetObjId(String value) {
        privateTargetObjId = value;
    }

    /**
     * 重载的相等运算符
     *
     * @param other
     * @return
     */
    protected final boolean equals(ViewModelMapping other) {
        return getMapType() == other.getMapType() && other.getTargetMetadataId().equals(getTargetMetadataId()) && other.getTargetMetadataPkgName().equals(getTargetMetadataPkgName()) && other.getTargetObjId().equals(getTargetObjId());
    }

    /**
     * 重写相等判断
     *
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        return equals((ViewModelMapping) obj);
    }

    /**
     * 创建作为当前实例副本的新对象。
     *
     * @return 作为此实例副本的新对象。
     *
     * <filterpriority>2</filterpriority>
     */
    public Object clone() {
        try {
            ViewModelMapping mapping = (ViewModelMapping) super.clone();
            return mapping;
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }

    /**
     * 返回表示当前 <see cref="T:System.Object"/> 的 <see cref="T:System.String"/>。
     *
     * @return <see cref="T:System.String"/>，表示当前的 <see cref="T:System.Object"/>。
     *
     * <filterpriority>2</filterpriority>
     */
    @Override
    public String toString() {
        return String.format("[MappingInfo]MapType: %1$s, TargetMetadataId: %2$s, TargetMetadataPkgName:%3$s, TargetObjId: %4$s", getMapType(), getTargetMetadataId(), getTargetMetadataPkgName(), getTargetObjId());
    }

    /**
     * 用作特定类型的哈希函数。
     *
     * @return 当前 <see cref="T:System.Object"/> 的哈希代码。
     *
     * <filterpriority>2</filterpriority>
     */
    @Override
    public int hashCode() {
        int hashCode = getMapType().getValue();
        hashCode = (hashCode * 397) ^ (getTargetMetadataId() != null ? getTargetMetadataId().hashCode() : 0);
        hashCode = (hashCode * 397) ^ (getTargetMetadataPkgName() != null ? getTargetMetadataPkgName().hashCode() : 0);
        hashCode = (hashCode * 397) ^ (getTargetObjId() != null ? getTargetObjId().hashCode() : 0);
        return hashCode;

    }
}
