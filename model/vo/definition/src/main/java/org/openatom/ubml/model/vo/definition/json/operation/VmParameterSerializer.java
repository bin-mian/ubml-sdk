/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.vo.definition.json.operation;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.openatom.ubml.model.common.definition.cef.json.SerializerUtils;
import org.openatom.ubml.model.common.definition.commonmodel.json.CommonModelNames;

import org.openatom.ubml.model.vo.definition.action.viewmodelbase.ViewModelParameter;
import org.openatom.ubml.model.vo.definition.json.ViewModelJsonConst;
/**
 * The Json Serializer Of View MOdel Action Parameter
 *
 * @ClassName: VmParameterSerializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class VmParameterSerializer<T extends ViewModelParameter> extends JsonSerializer<T> {
    @Override
    public void serialize(T value, JsonGenerator writer, SerializerProvider serializers){
        SerializerUtils.writeStartObject(writer);
        SerializerUtils.writePropertyValue(writer, CommonModelNames.ID, value.getID());
        SerializerUtils.writePropertyValue(writer, ViewModelJsonConst.ParamCode, value.getParamCode());
        SerializerUtils.writePropertyValue(writer, ViewModelJsonConst.ParamName, value.getParamName());
        SerializerUtils.writePropertyValue(writer, ViewModelJsonConst.ParameterType, value.getParameterType().toString());
        SerializerUtils.writePropertyValue(writer, ViewModelJsonConst.Assembly, value.getAssembly());
        SerializerUtils.writePropertyValue(writer, ViewModelJsonConst.JavaClassName, value.getClassName());
        SerializerUtils.writePropertyValue(writer, ViewModelJsonConst.ClassName, value.getDotnetClassName());
        SerializerUtils.writePropertyValue(writer, ViewModelJsonConst.Mode, value.getMode().toString());
        SerializerUtils.writePropertyValue(writer, ViewModelJsonConst.ParamDescription, value.getParamDescription());
        SerializerUtils.writePropertyValue(writer, ViewModelJsonConst.CollectionParameterType, value.getCollectionParameterType().toString());
        SerializerUtils.writePropertyValue(writer, ViewModelJsonConst.ParamActualValue, value.getActualValue());
        SerializerUtils.writeEndObject(writer);
    }
}
