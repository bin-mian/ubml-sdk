/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.vo.definition.json.operation;

import com.fasterxml.jackson.core.JsonGenerator;
import org.openatom.ubml.model.common.definition.cef.json.SerializerUtils;
import org.openatom.ubml.model.vo.definition.action.MappedCdpAction;
import org.openatom.ubml.model.vo.definition.action.ViewModelAction;
import org.openatom.ubml.model.vo.definition.json.ViewModelJsonConst;
/**
 * The Json Serializer Of Mapped Component Action Definition
 *
 * @ClassName: MappedCdpActionSerializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class MappedCdpActionSerializer extends VmActionSerializer<MappedCdpAction> {
    @Override
    protected VmParameterSerializer getParaConvertor() {
        return new MappedCdpParaSerializer();
    }

    @Override
    protected void writeExtendOperationProperty(JsonGenerator writer, ViewModelAction op) {
        MappedCdpAction action = (MappedCdpAction) op;
        SerializerUtils.writePropertyValue(writer, ViewModelJsonConst.ComponentEntityId, action.getComponentEntityId());
        SerializerUtils.writePropertyValue(writer, ViewModelJsonConst.ComponentPkgName, action.getComponentPkgName());
        SerializerUtils.writePropertyValue(writer, ViewModelJsonConst.IsGenerateComponent, action.getIsGenerateComponent());
    }
}
