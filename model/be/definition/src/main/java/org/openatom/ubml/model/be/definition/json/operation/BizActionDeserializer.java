/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.be.definition.json.operation;

import com.fasterxml.jackson.core.JsonParser;
import org.openatom.ubml.model.be.definition.operation.BizAction;
import org.openatom.ubml.model.be.definition.operation.BizActionBase;
import org.openatom.ubml.model.be.definition.operation.BizOperation;
import org.openatom.ubml.model.be.definition.operation.beomponent.BizActionParamCollection;
import org.openatom.ubml.model.be.definition.operation.componentbase.BizParameterCollection;

/**
 * The  Josn Deserializer Of Entity Action
 *
 * @ClassName: BizActionDeserializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */

public class BizActionDeserializer extends BizActionBaseDeserializer {
    @Override
    protected final boolean readExtendActionProperty(JsonParser jsonParser, BizOperation op, String propName) {
        return false;
    }

    @Override
    protected BizActionBase createBizActionBase() {
        return new BizAction();
    }

    @Override
    protected BizParaDeserializer createPrapConvertor() {
        return new BizActionParaDeserializer();
    }

    @Override
    protected BizParameterCollection createPrapCollection() {
        return new BizActionParamCollection();
    }
}
