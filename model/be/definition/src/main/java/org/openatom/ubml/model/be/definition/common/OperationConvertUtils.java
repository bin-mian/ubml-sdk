/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.be.definition.common;

import java.util.EnumSet;
import org.openatom.ubml.model.be.definition.beenum.BETriggerTimePointType;
import org.openatom.ubml.model.be.definition.beenum.RequestNodeTriggerType;
import org.openatom.ubml.model.be.definition.operation.BizOperation;
import org.openatom.ubml.model.be.definition.operation.Determination;
import org.openatom.ubml.model.be.definition.operation.Validation;
import org.openatom.ubml.model.be.definition.operation.collection.DeterminationCollection;
import org.openatom.ubml.model.be.definition.operation.collection.ValidationCollection;
import org.openatom.ubml.model.common.definition.cef.collection.CommonDtmCollection;
import org.openatom.ubml.model.common.definition.cef.collection.CommonValCollection;
import org.openatom.ubml.model.common.definition.cef.operation.CommonDetermination;
import org.openatom.ubml.model.common.definition.cef.operation.CommonOperation;
import org.openatom.ubml.model.common.definition.cef.operation.CommonValidation;
import org.openatom.ubml.model.common.definition.cef.operation.ExecutingDataStatus;

/**
 * The Convert Utils Of Be Operation
 *
 * @ClassName: OperationConvertUtils
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class OperationConvertUtils {
    /**
     * 转换联动计算列表
     *
     * @param dtms 联动计算集合
     * @param type 触发时机
     * @return
     */
    public static CommonDtmCollection convertToCommonDtms(DeterminationCollection dtms, BETriggerTimePointType type) {
        CommonDtmCollection cDtms = new CommonDtmCollection();
        for (BizOperation op : dtms) {
            Determination dtm = (Determination)op;
            if (dtm.getTriggerTimePointType().contains(type)) {
                cDtms.add(convertToCommonDtm(dtm));
            }
        }
        return cDtms;
    }

    /**
     * 转换校验规则列表
     *
     * @param vals
     * @return
     */
    public static CommonValCollection convertToCommonValidations(ValidationCollection vals, BETriggerTimePointType type) {
        CommonValCollection cVals = new CommonValCollection();
        for (BizOperation op : vals) {
            Validation val = (Validation)op;
            if (val.getValidationTriggerPoints().containsKey(type) && val.getValidationTriggerPoints().get(type).size() > 0) {
                cVals.add(convertToCommonValidation(val, type));
            }
        }
        return cVals;
    }

    // 私有方法
    public static CommonDetermination convertToCommonDtm(Determination dtm) {
        CommonDetermination cDtm = new CommonDetermination();
        convertToCommonOperation(cDtm, dtm);
        if (dtm.getRequestElements().size() > 0) {
            for (String item : dtm.getRequestElements()) {
                cDtm.getRequestElements().add(item);
            }
        }
        cDtm.setGetExecutingDataStatus(getExecutingDataStatus(dtm.getRequestNodeTriggerType()));
        return cDtm;
    }

    public static CommonValidation convertToCommonValidation(Validation validation, BETriggerTimePointType type) {
        CommonValidation cValidation = new CommonValidation();
        convertToCommonOperation(cValidation, validation);
        if (validation.getRequestElements().size() > 0) {
            for (String item : validation.getRequestElements()) {
                cValidation.getRequestElements().add(item);
            }
        }
        EnumSet<RequestNodeTriggerType> triggerTypes = validation.getValidationTriggerPoints().get(type);
        cValidation.setGetExecutingDataStatus(getExecutingDataStatus(triggerTypes));
        return cValidation;
    }

    /**
     * 触发时机转换
     *
     * @param enumSet
     * @return
     */
    private static EnumSet<ExecutingDataStatus> getExecutingDataStatus(EnumSet<RequestNodeTriggerType> enumSet) {
        EnumSet<ExecutingDataStatus> set = EnumSet.noneOf(ExecutingDataStatus.class);
        boolean isadded = enumSet.contains(RequestNodeTriggerType.Created);
        if (isadded) {
            set.add(ExecutingDataStatus.added);
        }
        boolean isModify = enumSet.contains(RequestNodeTriggerType.Updated);
        if (isModify) {
            set.add(ExecutingDataStatus.Modify);
        }
        boolean isDeleted = enumSet.contains(RequestNodeTriggerType.Deleted);
        if (isDeleted) {
            set.add(ExecutingDataStatus.Deleted);
        }
        return set;
    }

    /**
     * commonOperation基类属性转换
     *
     * @param commonOp
     * @param bizOp
     */
    private static void convertToCommonOperation(CommonOperation commonOp, BizOperation bizOp) {
        commonOp.setID(bizOp.getID());
        commonOp.setCode(bizOp.getCode());
        commonOp.setName(bizOp.getName());
        commonOp.setDescription(bizOp.getDescription());
        commonOp.setComponentId(bizOp.getComponentId());
        commonOp.setComponentName(bizOp.getComponentName());
        commonOp.setComponentPkgName(bizOp.getComponentPkgName());
    }

}