/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.be.definition.json.operation;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.deser.std.StringDeserializer;
import java.util.HashMap;
import org.openatom.ubml.model.be.definition.beenum.BEDeterminationType;
import org.openatom.ubml.model.be.definition.collection.DtmElementCollection;
import org.openatom.ubml.model.be.definition.common.BizEntityJsonConst;
import org.openatom.ubml.model.be.definition.operation.BizOperation;
import org.openatom.ubml.model.be.definition.operation.Determination;
import org.openatom.ubml.model.common.definition.cef.json.SerializerUtils;

/**
 * The  Josn Deserializer Of Entity Determination
 *
 * @ClassName: BizDeterminationDeserializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class BizDeterminationDeserializer extends BizOperationDeserializer<Determination> {
    @Override
    protected Determination createBizOp() {
        return new Determination();
    }

    @Override
    protected boolean readExtendOpProperty(BizOperation op, String propName, JsonParser jsonParser) {
        Determination dtm = (Determination)op;
        boolean hasProperty = true;
        switch (propName) {
            case BizEntityJsonConst.DETERMINATION_TYPE:
                dtm.setDeterminationType(SerializerUtils.readPropertyValue_Enum(jsonParser, BEDeterminationType.class, BEDeterminationType.values()));
                break;
            case BizEntityJsonConst.TRIGGER_TIME_POINT_TYPE:
                dtm.setTriggerTimePointType(readBETriggerTimePointType(jsonParser));
                break;
            case BizEntityJsonConst.REQUEST_NODE_TRIGGER_TYPE:
                dtm.setRequestNodeTriggerType(readRequestNodeTriggerType(jsonParser));
                break;
            case BizEntityJsonConst.REQUEST_ELEMENTS:
                dtm.setRequestElements(readElementArray(jsonParser));
                break;
            case BizEntityJsonConst.REQUEST_CHILD_ELEMENTS:
                dtm.setRequestChildElements(readRequestChildElements(jsonParser));
                break;
            case BizEntityJsonConst.PRE_DTM_ID:
                break;
            default:
                hasProperty = false;
        }
        return hasProperty;
    }

    private HashMap<String, DtmElementCollection> readRequestChildElements(JsonParser jsonParser) {
        RequestChildElementsDeserializer deserializer = new DtmRequestChildElementsDeserializer();
        return deserializer.deserialize(jsonParser, null);
    }

    private DtmElementCollection readElementArray(JsonParser jsonParser) {
        DtmElementCollection collection = new DtmElementCollection();
        SerializerUtils.readArray(jsonParser, new StringDeserializer(), collection, true);
        return collection;
    }
}
